<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
$lists = Yii::$app->params['list'];
$prices =  Yii::$app->params['prices'];
$this->title = 'Калькулятор';

?>

<div class="text-center mb-4 mt-3">
    <h1> Калькулятор стоимости доставки сырья </h1>
</div>
<?php Pjax::begin() ?>
<div class="row justify-content-center">
    <div class="col-md-6 border rounded-3 p-4 shadow ">
            <?php 
                $form = ActiveForm::begin([
                    'id' => 'calculate_form',
                    'method' => 'post',
                    'fieldConfig' => ['errorOptions' => ['encode' => false, 'class' => 'help-block', 'style'=>'color: red']]
                ]); 
            ?>
            <div class="mb-3 required">
                <?= $model->getDropDownList($form, 'month', $lists['months'], $lists['months'], 'Выберите месяц'); ?>
            </div>
            <div class="mb-3 required">
                <?= $model->getDropDownList($form, 'tonnage', $lists['tonnages'], $lists['tonnages'], 'Выберите тоннаж'); ?>
            </div>
            <div class="mb-3 required">
                <?=$model->getDropDownList($form, 'raw_type', $lists['raw_types'], $lists['raw_types'], 'Выберите тип сырья');?>
            </div>
            <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-success', 'id' => 'calculate_button']); ?>
            <?= Html::a('Сброс', Url::to(['']), ['class' => 'btn btn-danger']); ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php if (empty($_POST) === false): ?>

    <?php
    $price = findPrice(
        $_POST['month'],
        (int) $_POST['tonnage'],
        $_POST['raw_type'],
        $prices
    );
    ?>

    <div id="result" class="mb-4">
        <div class="row justify-content-center mt-5">
            <div class="col-md-3 me-3">
                <div class="card shadow-lg">
                    <div class="card-header bg-success text-white" style="font-weight: bold; font-size: 17px;">
                        Введенные данные:
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <strong> Месяц: </strong> <?= mb_convert_case($model->month, MB_CASE_TITLE, 'UTF-8') ?>
                        </li>
                        <li class="list-group-item">
                            <strong> Тоннаж: </strong> <?= mb_convert_case($model->tonnage, MB_CASE_TITLE, 'UTF-8') ?>
                        </li>
                        <li class="list-group-item">
                            <strong> Тип сырья: </strong> <?= mb_convert_case($model->raw_type, MB_CASE_TITLE, 'UTF-8') ?>
                        </li>
                        <li class="list-group-item">
                            <strong> Итог, руб.: </strong>
                            <?= $price ?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 table-responsive border rounded-1 shadow-lg mt-0 p-0">
                <table class="table table-hover table-striped text-center mb-0">
                    <thead>
                    <tr>
                        <th>Т/M</th>
                        <?php foreach (getPriceTonnages($model->raw_type, $prices) as $tonnage): ?>
                            <th><?= mb_convert_case($tonnage, MB_CASE_TITLE, 'UTF-8') ?></th>
                        <?php endforeach ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (getPriceMonths($model->raw_type, $prices) as $month): ?>
                        <tr>
                            <td>
                                <?= mb_convert_case($month, MB_CASE_TITLE, 'UTF-8') ?>
                            </td>
                            <?php foreach (getPriceByRawTypeAndMonth($model->raw_type, $month, $prices) as $tonnage => $price): ?>
                                <td class="<?= getStylesOnCondition($model->month, (int) $model->tonnage, $month, $tonnage) ?>">
                                    <?= $price ?>
                                </td>
                            <?php endforeach ?>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php endif ?>
<?php Pjax::end() ?>