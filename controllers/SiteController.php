<?php

namespace app\controllers;

use yii;
use yii\web\Controller;
use yii\web\ErrorAction;
use app\models\CalculatorForm;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'blanc';

        return $this->render('index');
    }

    public function actionCalculator() {
        $this->layout = 'calculator_layout';

        $model = new CalculatorForm();
        
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax ) { 
           
            // Do Something
            return $this->refresh();
        }
        
        return $this->render('form', compact('model'));
    }
}
