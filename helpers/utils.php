<?php

function getSelectedAttributeOnInputCondition(
    array $input,
    string $inputName,
    mixed $condition,
    bool $asInt = false
): ?string {

    if (isset($input[$inputName]) === false) {
        return null;
    }

    $condition = $asInt === true ? (int) $condition : (string) $condition;

    if ($input[$inputName] !== $condition) {

        return null;
    }

    return 'selected';
}

function getPriceTonnages(string $rawType, array $prices): array {

    if (isset($prices[$rawType]) === true) {

        $firstMonth = array_key_first($prices[$rawType]);

        return array_keys($prices[$rawType][$firstMonth]);
    }

    throw new \LogicException('Стоимости для типа сырья ' . $rawType . ' отсутствуют');
}

function getPriceMonths(string $rawType, array $prices): array {
    if (isset($prices[$rawType]) === true) {

        return array_keys($prices[$rawType]);
    }

    throw new \LogicException('Стоимости для типа сырья ' . $rawType . ' отсутствуют');
}

function getPriceByRawTypeAndMonth(string $rawType, string $month, array $prices): array {
    if (isset($prices[$rawType][$month]) === true) {

        return $prices[$rawType][$month];
    }

    throw new \LogicException('Стоимости для типа сырья ' . $rawType . ' и месяца ' . $month . ' отсутствуют');
}

function getStylesOnCondition(
    string $month,
    int $tonnage,
    string $conditionMonth,
    int $conditionTonnage
): ?string {

    if ($month !== $conditionMonth) {
        return null;
    }

    if ($tonnage !== $conditionTonnage) {
        return null;
    }

    return 'with-border';
}

function findPrice(string $month, int $tonnage, string $rawType, array $prices): int {

    if (isset($prices[$rawType][$month][$tonnage]) === true) {

        return $prices[$rawType][$month][$tonnage];
    }

    throw new \LogicException(
        'Стоимость для параметров месяц: ' . $month .
        ', тоннаж: ' . $tonnage .
        ', тип сырья: ' . $rawType . ' не найдена'
    );
}