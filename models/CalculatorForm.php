<?php

namespace app\models;

use yii\base\Model;
use yii\widgets\ActiveField;
use yii\widgets\ActiveForm;

class CalculatorForm extends Model
 {
    public $month;
    public $tonnage;
    public $raw_type;

    public function rules() 
    {
        return [
            [['month', 'tonnage', 'raw_type'], 'required', 'message' => "Обязательно к заполнению!"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'month' => 'Месяц',
            'tonnage' => 'Тоннаж',
            'raw_type' => 'Тип сырья',
        ];
    }

    /**
    * Переопределям имя формы.

    * Необходимо переопределить formName т.к в ином случае параметры в $_POST будут иметь вид CalculatorForm[param_name];

    * Избавляет нас от указания имени формы при вызове метода $model->load в контроллере.

    */
    public function formName(): string {
        return '';
    }

    // TODO: в дальнейшем функцию getDropDownList необходимо убрать отсюда.
    /**
    * Создает тэг select с заранее определенными настройками. 
    * Внутри себя функция содержит метод array_combine в который передаются массивы $keys и $values.

    * @param ActiveForm $form текущая форма ActiveForm из yii\widgets\ActiveForm;
    * @param string $attribute id и name тега select;
    * @param array $keys массив, содержащий ключи. Значения ключей будут подставленны в параметр "value" тэга "options";
    * @param array $values массив, содержащий значения. Значения данного массива будут подставлены между тэгом "options";
    * @param string $defaultText - текст первого элемента. По умолчанию выбран и недоступен. Контролируется переменными $selected и $disabled;
    * @param bool $selected настройка для $defaultText, по умолчанию включен. Элемент будет включен по умолчанию;
    * @param bool $disabled настройка для $defaultText, по умолчанию включен. Делает недоступным к выбору элемент.
    * @return созданный ActiveField объект.
    */
    function getDropDownList(
        ActiveForm $form, 
        string $attribute, 
        array $keys, 
        array $values, 
        string $defaultText = 'Выберите параметр',  
        bool $selected = true, 
        bool $disabled = true,
        array $styles = []
    ): ActiveField {
        return $form->field($this, $attribute)->dropDownList(array_combine($keys, $values),
        [
            'prompt' => [
                'text' => $defaultText ,
                'options' => [
                    'value' => '',
                    'selected' => $selected,
                    'disabled' => $disabled,
                    'style' => $styles,
                ]
            ]
        ]);
    }
}